{-# LANGUAGE InstanceSigs #-}
module Main where
import qualified Text.Pandoc as P
import qualified Text.Pandoc.Walk as PW
import Text.XML.Stream.Parse as XML
import qualified Data.Text as T
import qualified Data.List as L
import qualified Data.Maybe as My
import           Database.SQLite.Simple
import Conduit
import Data.XML.Types
import qualified Control.Monad as CM
import qualified Control.Concurrent as CC
import qualified Control.Concurrent.Async as A
import qualified Data.Either as E

data Page = Page {
  title :: T.Text,
  redirect :: Maybe T.Text,
  text :: T.Text
                 } deriving (Eq, Ord, Show)

instance ToRow Page where
  toRow :: Page -> [SQLData]
  toRow (Page title _ text) = toRow (title, text)

dropTables :: Query
dropTables = "drop table if exists wiki_articles"

schema :: Query
schema = "CREATE TABLE if not exists wiki_articles (title text not null, text text not null, pk INTEGER PRIMARY KEY not null) strict;"

-- https://phiresky.github.io/blog/2020/sqlite-performance-tuning/
databaseSettings :: [Query]
databaseSettings = [
  "pragma journal_mode = WAL;",
  "pragma synchronous = off;",
  "pragma temp_store = memory;",
  "pragma mmap_size = 30000000000;"
  ]


main :: IO ()
main = do
  conn <- open "wikipedia-dump.db"
  execute_ conn dropTables
  execute_ conn schema
  mapM_ (execute_ conn) databaseSettings
  let doc = stdinC .| parseBytes def
  runConduit $ doc .| manyYield' parsePage .| filterC (My.isNothing . redirect) .| mapM_C (forkRun conn)


forkRun :: Connection -> Page -> IO ()
forkRun conn page =  CM.void . CC.forkIO $ insertToTable conn page'
  where
    noLinkText = E.fromRight (text page) . P.runPure . removeLinks . text $ page
    page' = page {text = noLinkText}

removeLinks :: P.PandocMonad m  => T.Text -> m T.Text
removeLinks a = do
  asPandoc <- P.readMediaWiki P.def a
  P.writeMediaWiki def . PW.walk replaceLinks $ asPandoc
  where
    replaceLinks (P.Link attr' inlines _) = P.Span attr' inlines
    replaceLinks b = b

insertToTable :: ToRow p => Connection -> p -> IO ()
insertToTable conn = execute conn "insert into wiki_articles (title, text) values (?,?)"

parsePage :: ConduitT Event o IO (Maybe Page)
parsePage = do
  dropWhileC (\case {EventBeginElement (Name {nameLocalName = "page"}) _ -> False; _ -> True})
  eventsToPage <$> (CM.void (takeTree (matchNameOnly "page" ) ignoreAttrs) .| sinkList)

eventsToPage :: [Event] -> Maybe Page
eventsToPage events = case (title, text) of
  (Just title', Just text') -> Just (Page {title = title', text = text', redirect = redirect})
  _ -> Nothing
  where
    title :: Maybe T.Text
    title = \case {_:EventContent (ContentText title'):_ -> Just title'; _ -> Nothing}
        $ dropWhile (/= EventBeginElement (Name {nameLocalName = "title", nameNamespace = Nothing, namePrefix = Nothing}) []) events

    text :: Maybe T.Text
    text = \case {"" -> Nothing; a -> Just a}
           . (T.concat . L.intersperse "\n" . My.catMaybes . takeWhile My.isJust . map (\case {EventContent (ContentText text') -> Just text'; _ -> Nothing})
           . snd) =<<
            (L.uncons
           . dropWhile (\case {EventBeginElement (Name {nameLocalName = "text"}) _ -> False; _ -> True}) $ events)

    redirect :: Maybe T.Text
    redirect = My.listToMaybe . My.mapMaybe (\case
               {EventBeginElement (Name {nameLocalName = "redirect"}) [(Name {nameLocalName = "title"},[ContentText redirect'])] -> Just redirect';
                _ -> Nothing}) $ events

notMatching :: NameMatcher a -> NameMatcher Name
notMatching matcher = matching (My.isNothing . runNameMatcher matcher)
matchNameOnly :: T.Text -> NameMatcher Name
matchNameOnly s = matching ((==) s . nameLocalName)
